package com.kirdow.fireworker;

import java.io.File;

public class LaunchOptions {
	
	private static LaunchOptions options;
	public static LaunchOptions getOptions() {
		return options;
	}
	
	public final String script;
	public final int width;
	public final int height;
	
	public LaunchOptions(String script, int width, int height) {
		options = this;
		
		this.width = width;
		this.height = height;
		if (script != null && !(new File(script + ".fws")).exists())
			this.script = null;
		else
			this.script = script;
	}
	
	public static LaunchOptions fromArgs(String...args) {
		String _arg = null;
		
		String script;
		int width, height;
		
		script = null;
		width = 800;
		height = 600;
		
		for (int i = 0; i < args.length; i++) {
			String raw = args[i];
			String par = raw.toLowerCase();
			
			if (_arg == null) {
				switch(par) {
				case "-w":
					_arg = "width";
					break;
				case "-h":
					_arg = "height";
					break;
				default:
					script = raw;
					_arg = null;
					break;
				}
			} else {
				switch (_arg) {
				case "width":
					width = getInt(raw, 800);
					break;
				case "height":
					height = getInt(raw, 600);
					break;
				default:
					break;
				}
				_arg = null;
			}
		}
		
		return new LaunchOptions(script, width, height);
	}
	
	public static int getInt(String arg, int _default) {
		try {
			return Integer.parseInt(arg);
		} catch (NumberFormatException e) {
			return _default;
		}
	}
	
	public static int getWidth() {
		return options.width;
	}
	
	public static int getHeight() {
		return options.height;
	}
	
	public static String getScript() {
		return options.script;
	}
	
}
