package com.kirdow.fireworker.graphics;

public class Screen {
	
	public final int width, height;
	public final int[] pixels;
	
	public Screen(int width, int height) {
		this.width = width;
		this.height = height;
		this.pixels = new int[width*height];
	}
	
	/**
	 * Fade a color between 2 values
	 * @param src The source color
	 * @param dest The destination color
	 * @param factor The factor, 0.0 = source, 1.0 = destination
	 * @return The resulting color
	 */
	public int fade(int src, int dest, float factor) {
		int sr = ((src >> 16) & 0xFF);
		int sg = ((src >> 8) & 0xFF);
		int sb = ((src) & 0xFF);
		int sa = ((src >> 24) & 0xFF);
		
		int dr = ((dest >> 16) & 0xFF);
		int dg = ((dest >> 8) & 0xFF);
		int db = ((dest) & 0xFF);
		int da = ((dest >> 24) & 0xFF);
		
		double rd = dr - sr;
		double gd = dg - sg;
		double bd = db - sb;
		double ad = da - sa;
		
		rd *= factor;
		gd *= factor;
		bd *= factor;
		ad *= factor;
		
		int r = (int)(rd + sr);
		int g = (int)(gd + sg);
		int b = (int)(bd + sb);
		int a = (int)(ad + sa);
		
		return r << 16 | g << 8 | b | a << 24;
	}
	
	public void drawCircle(int x, int y, int r, int color) {
		final int r2 = r * r;
		int xp, yp, a, b, c2;
		for (a = -r; a <= r; a++) {
			xp = x + a;
			if (xp < 0 || xp >= width) continue;
			for (b = -r; b <= r; b++) {
				yp = y + b;
				if (yp < 0 || yp >= height) continue;
				
				c2 = a * a + b * b;
				if (c2 < r2)
					pixels[xp + yp * width] = color;
			}
		}
	}
	
	public void drawCircle(int x, int y, int r, int color, int fadeColor, FadeFilter filter) {
		if (filter == null) filter = (p, m) -> p / m;
		
		final int r2 = r * r;
		int xp, yp, a, b, c2, c;
		float f;
		for (a = -r; a <= r; a++) {
			xp = x + a;
			if (xp < 0 || xp >= width) continue;
			for (b = -r; b <= r; b++){
				yp = y + b;
				if (yp < 0 || yp >= height) continue;
				
				c2 = a * a + b * b;
				if (c2 < r2) {
					f = filter.apply((float)Math.sqrt(c2), r);
					
					c = fade(color, fadeColor, f);
					pixels[xp + yp * width] = c;
				}
			}
		}
	}
	
	public void set(int x, int y, int color) {
		if (x < 0 || y < 0 || x >= width || y >= height) return;
		pixels[x + y * width] = color;
	}
	
	public void set(double x, double y, int color) {
		set((int)x, (int)y, color);
	}
	
	public static interface FadeFilter {
		public float apply(float point, float max);
	}
	
}
