package com.kirdow.fireworker.scene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.kirdow.fireworker.firework.Firework;
import com.kirdow.fireworker.firework.FireworkCustom;

public class Script {
	
	public final boolean good;
	
	private String[] script;
	private Scene scene = null;
	
	private int index;
	
	public Script(String name) {
		File file = new File(name + ".fws");
		if (!file.exists()) {
			System.out.println("Could not find script!");
			good = false;
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while (reader.ready())
				sb.append(reader.readLine()).append("\n");
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("Could not read script, file not found!");
			good = false;
			return;
		} catch (IOException e) {
			System.out.println("could not read script!");
			good = false;
			return;
		}
		
		String content = sb.toString().trim().replace("\r\n", "\n");
		
		System.out.println("Got Script: \n-----------------\n" + content + "\n-----------------");
		
		this.script = content.split("\n");
		this.good = true;
	}
	
	public Script attach(Scene scene) {
		this.scene = scene;
		this.index = 0;
		return this;
	}
	
	public Script prepare() {
		if (scene != null)
			scene.clear();
		this.index = 0;
		return this;
	}
	
	private long wait = -1;
	
	public boolean next() {
		if (scene == null) return false;
		boolean res = _next();
		if (!res) {
			if (!scene.empty()) {
				wait = System.currentTimeMillis() + 100;
			} else {
				prepare();
				wait = System.currentTimeMillis() + 2500;
			}
		}
		return res;
	}
	
	private boolean _next() {
		if (wait > 0) {
			long now = System.currentTimeMillis();
			if (now < wait) return true;
			wait = -1;
		}
		if (index >= script.length) return false;
		if (scene == null) return false;
		String line = script[index++].trim();
		if (line.contains("#"))
			line = line.substring(0, line.indexOf("#")).trim();
		if (line.length() < 1) return true;
		
		String[] parts = line.split(" ");
		
		if (parts.length == 2) {
			System.out.println("Got 2 parts");
			if (parts[0].equals("spawn")) {
				System.out.println("Got spawn: " + parts[1]);
				scene.spawnRocket(parts[1]);
			} else if (parts[0].equals("wait")) {
				System.out.print("Got wait: ");
				long wait = 100;
				try {
					wait = Long.parseLong(parts[1]);
				} catch (NumberFormatException e) {}
				System.out.println("" + wait);
				this.wait = System.currentTimeMillis() + wait;
			} else {
				System.out.println("Failed!");
				return false;
			}
		} else if ((parts.length == 6 || parts.length == 8) && parts[0].equals("new")) {
			String name = parts[1];
			Integer color = getInt(parts[2]);
			Double light = getDouble(parts[3]);
			Double minSpeed = getDouble(parts[4]);
			Double maxSpeed = getDouble(parts[5]);
			Integer particleMin = 10;
			Integer particleVar = 5;
			if (parts.length == 8) {
				particleMin = getInt(parts[6]);
				particleVar = getInt(parts[7]);
			}
			if (color == null || light == null || minSpeed == null || maxSpeed == null || particleMin == null || particleVar == null) return false;
			new FireworkCustom(name, color, light, minSpeed, maxSpeed, particleMin, particleVar);
		} else {
			System.out.println("Failed!");
			return false;
		}
		return true;
	}
	
	private Integer getInt(String s) {
		try {
			if (s.startsWith("0x")) return Integer.parseInt(s.substring(2), 16) | 255 << 24;
			else if (s.startsWith("#")) return Integer.parseInt(s.substring(1), 16) | 255 << 24;
			return Integer.parseInt(s);
		} catch (NumberFormatException e) { System.out.println("Invalid integer: " + s); return null; }
	}
	
	private Double getDouble(String s) {
		try {
			return Double.parseDouble(s);
		} catch (NumberFormatException e) { System.out.println("Invalid double: " + s);return null; }
	}
	
}
