package com.kirdow.fireworker.scene;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.kirdow.fireworker.LaunchOptions;
import com.kirdow.fireworker.entity.Entity;
import com.kirdow.fireworker.entity.EntityParticle;
import com.kirdow.fireworker.entity.EntityRocket;
import com.kirdow.fireworker.firework.Firework;
import com.kirdow.fireworker.graphics.Screen;

public class Scene {
	
	private Screen screen;
	private List<Entity> entities;
	
	private Script script;
	
	public Scene(Screen screen) {
		this.screen = screen;
		
		this.entities = new ArrayList<Entity>();
		
		String scpt = LaunchOptions.getScript();
		if (scpt != null) {
			Script script = new Script(scpt);
			if (script.good) {
				this.script = script;
				this.script.attach(this);
			}
		}
	}
	
	public void tick() {
		double delta = delta();
		
		Set<Entity> rm = new HashSet<Entity>();
		Set<Entity> ad = new HashSet<Entity>();
		for (Entity e : entities) {
			e.tick(delta);
			if (e.isDead()) {
				if (e instanceof EntityRocket) {
					EntityRocket rocket = (EntityRocket)e;
					List<EntityParticle> p = rocket.spawnParticles();
					for (EntityParticle _p : p)
						ad.add(_p);
				}
				rm.add(e);
			}
		}
		for (Entity e : rm)
			entities.remove(e);
		for (Entity e : ad)
			entities.add(e);
		
		if (script != null) {
			script.next();
		}
	}
	
	public boolean hasScript() {
		return this.script != null;
	}
	
	public boolean empty() {
		return empty(0);
	}
	
	public boolean empty(int cap) {
		return entities.size() <= cap;
	}
	
	public void spawnRocket(String name) {
		Firework fw = Firework.getFromName(name);
		EntityRocket entity = new EntityRocket(fw);
		entities.add(entity);
	}
	
	public void draw() {
		for (Entity e : entities)
			e.draw(screen);
	}
	
	public void clear() {
		this.entities.clear();
		Firework.clear();
	}
	
	private long last = -1;
	private double delta() {
		long now = System.nanoTime();
		if (last == -1) {
			last = now;
			return 0.0;
		}
		long diff = now - last;
		last = now;
		return (double)diff / 1000000000.0;
	}
	
}
