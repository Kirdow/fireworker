package com.kirdow.fireworker;

public class Main {
	
	public static void main(String[] args) {
		LaunchOptions lo = LaunchOptions.fromArgs(args);
		Display display = new Display(lo);
		display.init();
		display.start();
	}
	
}
