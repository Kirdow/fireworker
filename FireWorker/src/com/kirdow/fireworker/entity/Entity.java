package com.kirdow.fireworker.entity;

import java.util.UUID;

import com.kirdow.fireworker.graphics.Screen;

public abstract class Entity {
	
	public final UUID uuid;
	
	public double vx, vy;
	public double x, y;
	
	public boolean alive = true;
	
	public Entity() {
		this.x = 0;
		this.y = 0;
		this.uuid = UUID.randomUUID();
	}
	
	public Entity(double x, double y) {
		this.x = x;
		this.y = y;
		this.uuid = UUID.randomUUID();
	}
	
	public Entity setVelocity(double x, double y) {
		this.vx = x;
		this.vy = y;
		return this;
	}
	
	public Entity setPosition(double x, double y) {
		this.x = x;
		this.y = y;
		return this;
	}
	
	public void tick(double delta) {
		this.x += vx * delta;
		this.y += vy * delta;
	}
	
	public void draw(Screen screen) { }
	
	public boolean isAlive() {
		return this.alive;
	}
	
	public boolean isDead() {
		return !this.alive;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Entity)) return false;
		Entity other = (Entity)obj;
		return this.uuid.equals(other.uuid);
	}
	
	@Override
	public int hashCode() {
		final int BASE = 17;
		final int MULTIPLIER = 31;
		
		int result = BASE;
		
		result = MULTIPLIER * result + this.uuid.hashCode();
		
		return result;
	}
	
}
