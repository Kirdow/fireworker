package com.kirdow.fireworker.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.kirdow.fireworker.LaunchOptions;
import com.kirdow.fireworker.firework.Firework;
import com.kirdow.fireworker.graphics.Screen;

public class EntityRocket extends Entity {
	
	public final Firework type;
	
	public EntityRocket(Firework firework) {
		super();
		this.type = firework;
		Random r = new Random();
		double rot = r.nextDouble() * 60.0 - 30.0;
		this.y = LaunchOptions.getHeight();
		this.x = (double)LaunchOptions.getWidth() * (r.nextDouble() * 0.5 + 0.25);
		
		this.vx = rot;
		double s = r.nextDouble() * (type.maxSpeed - type.minSpeed);
		this.vy = -(type.minSpeed + s);
	}
	
	@Override
	public void tick(double delta) {
		super.tick(delta);
		
		if (y <= LaunchOptions.getHeight() / 3) {
			this.alive = false;
		}
	}
	
	@Override
	public void draw(Screen screen) {
		
		for (int y = -1; y < 8; y++) {
			for (int x = -1; x <= 1; x++) {
				if (y < 0 && x != 0) continue;
				screen.set(this.x + x, this.y + y, 0xFFFFFFFF);
			}
		}
		for (int y = -1; y <= 1; y++)
			for (int x = -1; x <= 1; x++) {
				if (!(x == 0 || y == 0)) continue;
				screen.set(this.x + x, this.y + 8 + y, spark());
			}
		
	}
	
	private static final int[] sparks = new int[]{0xFFFFDB3A, 0xFFFFF3BF, 0xFFFF221E, 0xFFFFD93A};
	private static int spark() {
		return sparks[new Random().nextInt(sparks.length)];
	}
	
	public List<EntityParticle> spawnParticles() {
		List<EntityParticle> particles = new ArrayList<EntityParticle>();
		int quantity = type.newQuantity();
		for (int i = 0; i < quantity; i++)
			particles.add(new EntityParticle(type, this));
		return particles;
	}
	
}
