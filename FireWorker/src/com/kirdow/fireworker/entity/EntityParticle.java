package com.kirdow.fireworker.entity;

import java.util.Random;

import com.kirdow.fireworker.firework.Firework;
import com.kirdow.fireworker.graphics.Screen;

public class EntityParticle extends Entity {
	
	private static final Random r = new Random();
	
	public final Firework type;
	
	private double life = 2.0;
	
	public EntityParticle(Firework firework, EntityRocket parent) {
		this.type = firework;
		this.x = parent.x;
		this.y = parent.y;
		
		this.life += r.nextDouble() * 1.6 - 0.8;
		
		double rot = r.nextDouble() * Math.PI * 2.0;
		double speed = 50.0 + r.nextDouble() * 240.0;
		
		double rx = Math.sin(rot);
		double ry = -Math.cos(rot);
		
		this.vx = rx * speed;
		this.vy = ry * speed;
	}
	
	@Override
	public void tick(double delta) {
		super.tick(delta);
		
		this.vx *= 1.0 - (0.99 * delta);
		this.vy *= 1.0 - (0.99 * delta);
		
		life -= delta;
		
		if (life <= 0.0) alive = false;
	}
	
	@Override
	public void draw(Screen screen) {
		screen.drawCircle((int)this.x, (int)this.y, 8, type.color, 0xFF000000, (p, m) -> {
			final double pow = type.light;
			return (float)(Math.pow(pow, p) / Math.pow(pow, m));
		});
	}

}
