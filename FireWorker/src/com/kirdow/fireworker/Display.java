package com.kirdow.fireworker;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

import javax.swing.JFrame;

import com.kirdow.fireworker.graphics.Screen;
import com.kirdow.fireworker.scene.Scene;

public class Display extends Canvas {
	
	public final LaunchOptions options;
	
	private JFrame frame;
	
	private Thread mainThread;
	private boolean running;
	
	private BufferedImage image;
	private int[] pixels;
	private Screen screen;
	
	private Scene scene;
	
	public Display(LaunchOptions lo) {
		this.options = lo;
		System.out.println(String.format("Got options\nwidth: %s\nHeight: %s\nScript: %s", lo.width, lo.height, lo.script));
		
		Dimension dim = new Dimension(lo.width, lo.height);
		this.setMinimumSize(dim);
		this.setMaximumSize(dim);
		this.setPreferredSize(dim);
		this.setSize(dim);
	}
	
	public void init() {
		setupWindow();
		setupCanvas();
	}
	
	private void setupWindow() {
		JFrame frame = new JFrame("FireWorker");
		frame.add(this);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	private void setupCanvas() {
		image = new BufferedImage(options.width, options.height, BufferedImage.TYPE_INT_ARGB);
		pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
		screen = new Screen(options.width, options.height);
	}
	
	public void start() {
		if (running) return;
		mainThread = new Thread(()-> {
			this.run();
		});
		running = true;
		mainThread.start();
	}
	
	public void stop() {
		if (!running) return;
		new Thread(()-> {
			running = false;
			try {
				mainThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();;
	}
	
	private void run() {
		scene = new Scene(screen);
		
		while (running) {
			tick();
			draw();
		}
	}
	
	private void tick() {
		if (!scene.hasScript()) {
			if (scene.empty()) {
				scene.spawnRocket(new Random().nextBoolean() ? "blue" : "green");
			}
		}
		
		scene.tick();
	}
	
	private void draw() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			this.requestFocus();
			return;
		}
		
		Arrays.fill(screen.pixels, 0xFF000000);
		
		// Draw Start
		
		scene.draw();
		
		// Draw End
		
		for (int i = 0; i < pixels.length; i++)
			pixels[i] = screen.pixels[i];
		
		Graphics g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, options.width, options.height, null);
		g.dispose();
		bs.show();
		
	}
	
	
	
}
