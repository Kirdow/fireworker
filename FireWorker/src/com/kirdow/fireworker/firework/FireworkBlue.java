package com.kirdow.fireworker.firework;

import java.util.Random;

public class FireworkBlue extends Firework {

	public FireworkBlue() {
		super("blue", 0xFF0000FF, 1.6, 240.0, 300.0);
	}
	
	@Override
	public int newQuantity() {
		return new Random().nextInt(10) + 12;
	}
	
}
