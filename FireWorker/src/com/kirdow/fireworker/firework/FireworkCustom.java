package com.kirdow.fireworker.firework;

import java.util.Random;

public class FireworkCustom extends Firework {
	
	private static final Random R = new Random();
	
	private int quantityMin, quantityVar;
	
	public FireworkCustom(String name, int color, double light, double minSpeed, double maxSpeed) {
		this(name, color, light, minSpeed, maxSpeed, 10, 5);
	}
	
	public FireworkCustom(String name, int color, double light, double minSpeed, double maxSpeed, int quantityMin, int quantityVar) {
		super(name, color, light, minSpeed, maxSpeed);
		
		this.quantityMin = quantityMin;
		this.quantityVar = quantityVar;
	}
	
	@Override
	public int newQuantity() {
		return R.nextInt(quantityVar) + quantityMin;
	}
	
	
}
