package com.kirdow.fireworker.firework;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Firework {
	
	private static final Set<Firework> standard;
	
	private static final Map<String, Firework> fireworks;
	@SuppressWarnings("unchecked")
	public static <T extends Firework> T getFromName(String name) {
		Firework firework = fireworks.get(name);
		if (firework == null) return null;
		try {
			return (T) firework;
		} catch (ClassCastException e) {
			return null;
		}
	}
	
	public static void clear() {
		fireworks.clear();
		for (Firework f : standard)
			fireworks.put(f.name, f);
	}
	
	
	public final int color;
	public final double light;
	public final double minSpeed, maxSpeed;
	public final String name;
	
	public Firework(String name, int color, double light, double minSpeed, double maxSpeed) {
		this.color = color;
		this.light = light;
		this.minSpeed = minSpeed;
		this.maxSpeed = maxSpeed;
		this.name = name;
		fireworks.put(name, this);
	}
	
	public abstract int newQuantity();
	
	public static final Firework blue;
	public static final Firework green;
	
	static {
		fireworks = new HashMap<String, Firework>();
		
		Set<Firework> std = new HashSet<Firework>();
		blue = new FireworkBlue();
		green = new FireworkGreen();
		std.add(blue);
		std.add(green);
		standard = Collections.unmodifiableSet(std);
	}
	
}
