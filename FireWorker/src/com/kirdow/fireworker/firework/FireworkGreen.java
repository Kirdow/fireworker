package com.kirdow.fireworker.firework;

import java.util.Random;

public class FireworkGreen extends Firework {
	
	public FireworkGreen() {
		super("green", 0xFF00FF00, 1.8, 240.0, 400.0);
	}
	
	@Override
	public int newQuantity() {
		return new Random().nextInt(18) + 8;
	}
	
}
