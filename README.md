# FireWorker #

This is the repository for my Open Source project FireWorker which is a Firework Simulator made from scratch in Java initially in just 4 hours

For reference, this is the inital source after the first 4 hours: [Link](https://bitbucket.org/Kirdow/fireworker/src/b559906478bb33a2b46116190ca796d98c3dae62/?at=initial_version)

### What is this repository for? ###

* This repository contains the initial source after the first 4 hours and the continued work of the software
* Version 1.0

### How do I use this software? ###

* Download zipfile.
* Unzip all files in a folder.
* Launch one of the batch files.

* Requires Java 1.8

### How do I contribute? ###

* At this point I'm not accepting contributions, I'm just having the source open since I'm doing this for fun and want the source visible for other to use.